# -*- coding: utf-8 -*-
# :Project:   Bean config — thinkpad specific config
# :Created:   ven 30 ago 2024, 21:41:04
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2024 Alberto Berti
#

{ ... }: {
  imports = [
    ./thinkpad/hardware-configuration.nix
    ./thinkpad/net.nix
    ./thinkpad/sleep.nix
    ./thinkpad/tlp.nix
  ];
}
