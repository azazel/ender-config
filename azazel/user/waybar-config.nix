# converted by https://json-to-nix.pages.dev/
{pkgs}: {
  layer = "top";
  position = "bottom";
  height = 26;
  modules-left = [
    "sway/workspaces"
    "custom/right-arrow-dark"
    "sway/mode"
    "sway/scratchpad"
];
  modules-center = [
    "custom/left-arrow-dark"
    "clock#1"
    "custom/left-arrow-light"
    "custom/left-arrow-dark"
    "clock#2"
    "custom/right-arrow-dark"
    "custom/right-arrow-light"
    "clock#3"
    "custom/right-arrow-dark"
  ];
  modules-right = [
    "custom/left-arrow-dark"
    "idle_inhibitor"
    "custom/left-arrow-light"
    "custom/left-arrow-dark"
    "network"
    "custom/left-arrow-light"
    "custom/left-arrow-dark"
    "pulseaudio"
    "custom/left-arrow-light"
    "custom/left-arrow-dark"
    "memory"
    "custom/left-arrow-light"
    "custom/left-arrow-dark"
    "cpu"
    "custom/left-arrow-light"
    "custom/left-arrow-dark"
    "temperature"
    "custom/left-arrow-light"
    "custom/left-arrow-dark"
    "battery"
    "custom/left-arrow-light"
    "custom/left-arrow-dark"
    "disk"
    "custom/left-arrow-light"
    "custom/left-arrow-dark"
    "tray"
  ];
  "custom/left-arrow-dark" = {
    format = "";
    tooltip = false;
  };
  "custom/left-arrow-light" = {
    format = "";
    tooltip = false;
  };
  "custom/right-arrow-dark" = {
    format = "";
    tooltip = false;
  };
  "custom/right-arrow-light" = {
    format = "";
    tooltip = false;
  };

  "sway/scratchpad" = {
    format = "{icon} {count}";
    show-empty = false;
    format-icons = [
      ""
      ""
    ];
    tooltip = true;
    tooltip-format = "{app}: {title}";
  };
  "sway/mode" = {
    format = "<span style=\"italic\">{}</span>";
  };
  "sway/workspaces" = {
    disable-scroll = true;
    format = "{name}";
  };


  battery = {
    states = {
      good = 75;
      warning = 30;
      critical = 15;
    };
    format = "{icon} {capacity}%";
    format-full = "{icon} {capacity}%";
    format-charging = "{icon} {capacity}% ";
    format-plugged = "{icon} {capacity}% ";
    format-alt = "{icon} {time}";
    format-icons = [
      ""
      ""
      ""
      ""
      ""
    ];
  };
  "clock#1" = {
    format = "{:%a}";
    tooltip-format = "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>";
    locale = "it_IT.UTF-8";
  };
  "clock#2" = {
    format = "{:%H:%M}";
    tooltip = false;
    locale = "it_IT.UTF-8";
  };
  "clock#3" = {
    format = "{:%d-%m}";
    tooltip-format = "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>";
    locale = "it_IT.UTF-8";
  };
  cpu = {
    interval = 5;
    format = " {load:2.1f} {usage:2}%";
  };
  disk = {
    interval = 5;
    format = "󰋊 {percentage_used:2}%";
    path = "/";
  };
  idle_inhibitor = {
    format = "{icon}";
    format-icons = {
      activated = "";
      deactivated = "";
    };
  };
  memory = {
    interval = 5;
    format = " {}%";
  };
  network = {
    format-wifi = "{essid} ({signalStrength}%) ";
    format-ethernet = "{ipaddr}/{cidr} 󰈀";
    tooltip-format = "{ifname} via {gwaddr} ";
    format-linked = "{ifname} (No IP) 󰌘";
    format-disconnected = "Disconnected ⚠";
    format-alt = "{ifname}: {ipaddr}/{cidr}";
  };
  pulseaudio = {
    format = "{icon} {volume:2}% {format_source}";
    format-bluetooth = "{icon} {volume}%󰂯 {format_source}";
    format-bluetooth-muted = "󰝟 {icon} {volume}%󰂯 {format_source}";
    format-muted = "󰝟 {format_source}";
    format-source = " {volume}%";
    format-source-muted = "";
    format-icons = {
      headphones = "";
      hands-free = "󰋎";
      headset = "󰋎";
      phone = "";
      portable = "";
      car = "";
      default = [
        ""
        ""
        ""
      ];
    };
    scroll-step = 5;
    on-click = "${pkgs.pamixer}/bin/pamixer -t";
    on-click-right = "${pkgs.pavucontrol}/bin/pavucontrol";
  };
  temperature = {
    interval = 5;
    thermal-zone = 4;
    critical-threshold = 90;
    format = "{icon} {temperatureC}°C";
    format-icons = [
      ""
      ""
      ""
    ];
  };
  tray = {
    icon-size = 20;
  };
}
