# See available options at https://nix-community.github.io/home-manager/options.html
{ config, lib, pkgs, nixosConfig, ... }: {
  imports = [
    ./git.nix
    ./gpg.nix
    ./programs.nix
    ./session.nix
    ./wayland.nix
  ];
  home.stateVersion = "22.05";
 }
