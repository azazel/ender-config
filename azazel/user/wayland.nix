{ config, lib, pkgs, nixosConfig,  ... }:
  let
    inherit (lib) mkIf;
    inherit (pkgs) writeShellApplication;
    waylandEnabled = nixosConfig.system.useWayland;
    terminal = prog "foot";
    prog = name: "${pkgs.${name}}/bin/${name}";
    swayProg = name: "${pkgs.sway}/bin/${name}";
    app = config: "${writeShellApplication config}/bin/${config.name}";
    script = name: text: app { inherit name text; runtimeInputs = [];};
    dbus-sway-environment = script "dbus-sway-environment" ''
      dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=sway
      systemctl --user stop pipewire pipewire-media-session xdg-desktop-portal xdg-desktop-portal-wlr
      systemctl --user start pipewire pipewire-media-session xdg-desktop-portal xdg-desktop-portal-wlr
    '';
    # Locking timeout in seconds, set to 0 to disable locking
    sessionLockTimeout = 600;
    mod = "Mod4";
  in {
    imports = [
      ./waybar.nix
    ];
    home.file = {
      networkmanager_dmenu-conf = {
        target = ".config/networkmanager-dmenu/config.ini";
        text = ''
          [dmenu]
          dmenu_command = bemenu
          [editor]
          terminal = ${terminal}
        '';
      };
    };
    programs = mkIf waylandEnabled {
    };
    services = mkIf waylandEnabled {
      gammastep = {
        enable = true;
        # my place in Trentino;
        latitude = "45.89";
        longitude = "11.04";
      };
      mako = {
        enable = true;
        defaultTimeout = 5000;
      };
    };
    wayland= {
      windowManager.sway =
        let
          internal-lcd = "eDP-1";
          lockCmd = "${prog "swaylock"} -c 000000";
        in {
          enable = waylandEnabled;
          config =
            let
              bctl = "${prog "brightnessctl"}";
              fname = "monospace";
              font = {
                names = [ fname ];
                size = 14.0;
              };
              gnome-schema = "org.gnome.desktop.interface";
              genWorkspaceKey = keylist: cmd:
                /* given ["${mod}" "Shift"] "workspace" generates an attrs with
                  {
                    "${mod}+Shift+1" = "workspace 1";
                    ...
                    "${mod}+Shift+0" = "workspace 10";
                  }
                  for all the 10 workspaces
                */
                let
                  inherit (lib) concatStringsSep genList listToAttrs;
                  wsFromIx = ix: if ix == 0 then 10 else ix;
                  keyFromIx = ix: if ix == 10 then 0 else ix;
                in
                  listToAttrs (
                    genList (ix:
                      let
                        ix_1 = ix + 1;
                      in {
                        name = "${concatStringsSep "+" (keylist ++ [(toString (keyFromIx ix_1))])}";
                        value = "${cmd} ${toString (wsFromIx ix_1)}";
                      }
                    ) 10);
              lg-5k = "Goldstar Company Ltd LG HDR 5K 904NTTQ6N889";
              menu = "${pkgs.bemenu}/bin/bemenu-run -b -p » --fn 'pango:${fname} ${builtins.toString font.size}'";
              smsg = "${swayProg "swaymsg"}";
              pactl = "${pkgs.pulseaudio}/bin/pactl";
              toggleGammastep = script "toggle-gammastep" ''
                if [ "$(${pkgs.procps}/bin/pgrep gammastep)" ]; then
                  systemctl --user stop gammastep
                else
                  systemctl --user start gammastep
                fi
              '';
              networkManagerMenu = app {
                name = "network-manager-menu";
                runtimeInputs = with pkgs; [
                  bemenu
                  networkmanager_dmenu
                ];
                text = ''
                  networkmanager_dmenu -b -p » --fn 'pango:${fname} ${builtins.toString font.size}'
                '';
              };
              configDisplays = "exec ${pkgs.wdisplays}/bin/wdisplays";
              screenshotWindow = app {
                name = "screenshot-window";
                runtimeInputs = with pkgs; [
                  grim
                  jq
                  slurp
                  swappy
                  sway
                ];
                text = ''
                  grim -g "$(swaymsg -t get_tree \
                              | jq -r '.. | select(.pid? and .visible?) | .rect | "\(.x),\(.y) \(.width)x\(.height)"' \
                              | slurp)" - \
                    | swappy -f -
                '';
              };
              screenshotDisplay = app {
                name = "screenshot-display";
                runtimeInputs = with pkgs; [
                  grim
                  jq
                  swappy
                  sway
                ];
                text = ''
                  output_id="$(swaymsg -t get_outputs | jq -r '.[] | select(.focused).name')"
                  grim -o "$output_id" - | swappy -f -
                '';
              };
              zoomIn = script "zoom-in" ''
                display_scale="$(${pkgs.sway}/bin/swaymsg -t get_outputs | ${pkgs.jq}/bin/jq -r '.. | select(.focused?) | .scale')"
                scale="$(echo "$display_scale"+.1 | ${pkgs.bc}/bin/bc -l)"
                [[ $(echo "''${scale} < 5" | ${pkgs.bc}/bin/bc -l) == 1 ]] \
                   && ${pkgs.sway}/bin/swaymsg "output * scale ''${scale}"
              '';
              zoomOut = script "zoom-out" ''
                display_scale="$(${pkgs.sway}/bin/swaymsg -t get_outputs | ${pkgs.jq}/bin/jq -r '.. | select(.focused?) | .scale')"
                scale="$(echo "$display_scale"-.1 | ${pkgs.bc}/bin/bc -l)"
                [[ $(echo "''${scale} > .25" | ${pkgs.bc}/bin/bc -l) == 1 ]] \
                  && ${pkgs.sway}/bin/swaymsg "output * scale ''${scale}"
              '';
              waybar = script "mywaybar"  ''
                exec ${config.programs.waybar.package}/bin/waybar -l info
              '';
            in {
              bars = [{
                  #statusCommand = "${pkgs.i3status}/bin/i3status";
                  #trayOutput = "*";
                  command = "${waybar}";
                  position = "bottom";
                }];
              fonts = font;
              input = {
                "*" = {
                  xkb_layout = "it";
                  xkb_options = "eurosign:e";
                };
              };
              # move keybindings, all configured here
              down = "k";
              left = "j";
              right = "ograve";
              up = "l";
              keybindings = {
                #
                # Move workspaces to other displays
                #

                "${mod}+Ctrl+Down" = "move workspace to output down";
                "${mod}+Ctrl+Left" = "move workspace to output left";
                "${mod}+Ctrl+Right" = "move workspace to output right";
                "${mod}+Ctrl+Up" = "move workspace to output up";

                #
                # Windows movement and control
                #

                "${mod}+Shift+Down" = "move down";
                "${mod}+Shift+Left" = "move left";
                "${mod}+Shift+Right" = "move right";
                "${mod}+Shift+Up" = "move up";

                # Move a window to scratchpad
                "${mod}+Shift+minus" = "move scratchpad";
                # Yank a window from scratchpad
                "${mod}+minus" = "scratchpad show";
                # Delete a window
                "${mod}+Shift+q" = "kill";
                # Show a window fullscreen
                "${mod}+f" = "fullscreen";
                # Togge a window to be visible in all the workspaces,
                # if floating
                "${mod}+g" = "sticky toggle";
                # Resize a window
                "${mod}+r" = ''mode "resize"'';
                # Move a window to the floating, non managed layer and back
                "${mod}+Shift+space" = "floating toggle";

                #
                # Managed layout management
                #

                "${mod}+e" = "layout toggle split";
                "${mod}+h" = "split h";
                "${mod}+s" = "layout stacking";
                "${mod}+w" = "layout tabbed";
                "${mod}+v" = "split v";
                "${mod}+a" = "focus parent";

                #
                # Move focus
                #

                "${mod}+Down" = "focus down";
                "${mod}+Left" = "focus left";
                "${mod}+Right" = "focus right";
                "${mod}+Up" = "focus up";
                "${mod}+space" = "focus mode_toggle";

                #
                # Start programs
                #

                # Run file manager
                "${mod}+Ctrl+b" = "exec caja";
                # Open chromium
                "${mod}+Ctrl+c" = "exec chrome";
                # Open Emacs
                "${mod}+Ctrl+e" = "exec emacs";
                # Run a verision of emacs configured for mail
                "${mod}+Ctrl+Shift+e" = "exec emacs --with-profile gnus -f mine-emacs";
                # Open Firefox
                "${mod}+Ctrl+f" = "exec firefox";
                # Open Kodi
                "${mod}+Ctrl+k" = "exec kodi";
                # Open Nyxt browser
                "${mod}+Ctrl+n" = "exec nyxt-ok.sh";
                # Open Signal
                "${mod}+Ctrl+s" = "exec signal";
                # Run a terminal
                "${mod}+Return" = "exec ${terminal}";

                #
                # Run configuration programs
                #

                # Configure displays position
                "${mod}+Shift+d" = configDisplays;
                XF86Display = configDisplays;
                # Show dialog to quit Sway
                "${mod}+Shift+e" = "exec ${swayProg "swaynag"} -m 'Exit Sway?' -b 'Yes, exit Sway' '${smsg} exit'";
                # Toggle the service that protects eyes from fatigue on the
                # evenings
                "${mod}+Shift+g" = "exec ${toggleGammastep}";
                # Lock the session
                # This is commented here becaue we need --no-repeat option, see
                # "extraConfig" section
                # "${mod}+Shift+l" = "exec ${lockCmd}";

                #
                # Menus
                #

                # Connect to a Wi-Fi
                "${mod}+Shift+w" = "exec ${networkManagerMenu}";
                # Start a program
                "${mod}+p" = "exec ${menu}";

                #
                # Configuration
                #

                "${mod}+Shift+c" = "reload";
                "${mod}+Shift+r" = "restart";

                #
                # Session zoom in/out
                #

                # Set scale to 1
                "${mod}+Ctrl+0" = "output * scale 1";
                # Zoom in
                "${mod}+Ctrl+plus" = "exec ${zoomIn}";
                # Zoom out
                "${mod}+Ctrl+minus"  = "exec ${zoomOut}";

                #
                # Media controls
                #

                # Volume down 5%
                XF86AudioLowerVolume = "exec ${pactl} set-sink-volume @DEFAULT_SINK@ -5%";
                # Mute/unmute volume
                XF86AudioMute = "exec ${pactl} set-sink-mute @DEFAULT_SINK@ toggle";
                # Volume up 5%
                XF86AudioRaiseVolume = "exec ${pactl} set-sink-volume @DEFAULT_SINK@ +5%";
                # Brightness down 2%
                XF86MonBrightnessDown = "exec ${bctl} s 2%-";
                # Brightness up 2%
                XF86MonBrightnessUp = "exec ${bctl} s 2%+";

                #
                # Screenshots
                #
                # Snip a selection and pipe to swappy
                Print = ''
                  exec ${prog "grim"} -g "$(${prog "slurp"})" - | ${prog "swappy"} -f -
                '';
                # Screenshot a window and pipe to swappy
                "Ctrl+Print" =  "exec ${screenshotWindow}";
                # Screenshot the current display and pipe to swappy
                "Shift+Print" =  "exec ${screenshotDisplay}";
              } // (genWorkspaceKey ["${mod}"] "workspace")
                // (genWorkspaceKey ["${mod}" "Shift"] "move container to workspace");
              menu = "${menu}";
              modifier = "${mod}";
              output = {
                ${internal-lcd} = {
                  mode = "1920x1200";
                  scale = "1";
                  subpixel = "vrgb";
                };
                ${lg-5k} = {
                  mode = "5120x2160";
                  # scale = "1.3";
                };
              };
              seat = {
                "*" = {
                  hide_cursor = "when-typing enable";
                  xcursor_theme = ''"OpenZone_Ice" 48'';
                };
              };
              startup = [
                { always = true;
                  command = "${dbus-sway-environment}";
                }{
                  # see https://nixos.wiki/wiki/Firefox
                  always = true;
                  command = "systemctl --user import-environment";
                }{
                  always = true;
                  command = "${pkgs.glib}/bin/gsettings set ${gnome-schema} text-scaling-factor 1.5";
                }
                # start polkit pinentry for priviledge escalation (virt-manager) also used
                # by gnome utils like nm-connection-editor
                { command = "${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1"; }

                # needs "programs.blueman" enables
                { command = "blueman-applet"; }
                { command = "${pkgs.nextcloud-client}/bin/nextcloud"; }
              ] ++ (lib.optional (sessionLockTimeout > 0) {
                  always = true;
                  command = ''
                    ${prog "swayidle"} -w \
                      timeout ${toString sessionLockTimeout} '${lockCmd} -f ' \
                      before-sleep '${lockCmd} -f'
                  '';
                });
              workspaceAutoBackAndForth = true;
            };
          extraConfig =
            let
              concat = builtins.concatStringsSep;
              enterIdleLock = if (sessionLockTimeout > 0)
                              then "${pkgs.procps}/bin/pkill -USR1 swayidle"
                              else lockCmd;
            in ''
              ${concat "\n" (map (key: "bindsym --no-repeat ${key} exec ${enterIdleLock}")
                [ "XF86Search" "XF86Tools" "${mod}+Shift+l" ])}
              bindswitch --reload --locked lid:on output ${internal-lcd} disable
              bindswitch --reload --locked lid:off output ${internal-lcd} enable
              default_border normal
              default_floating_border pixel
              xwayland enable
            '';
          extraSessionCommands = ''
            # SDL:
            export SDL_VIDEODRIVER=wayland
            # QT (needs qt5.qtwayland in systemPackages), needed by VirtualBox GUI:
            export QT_QPA_PLATFORM=wayland-egl
            export QT_WAYLAND_DISABLE_WINDOWDECORATION="1"
            export EDITOR=emacsclient
          '';
          wrapperFeatures = {
            base = true;
            gtk = true;
          };
        };
    };
  }
