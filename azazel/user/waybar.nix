{ config, lib, pkgs, nixosConfig,  ... }:
  let
    inherit (builtins) readFile;
  in {
    programs.waybar = {
      enable = true;
      style = readFile ./waybar.css;
      settings.main = (import ./waybar-config.nix {inherit pkgs;}) // {
        layer = "top";
        position = "bottom";
        reload_style_on_change = true;
      };
  };
}
