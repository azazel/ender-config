# See available options at https://nix-community.github.io/home-manager/options.html
{ config, lib, pkgs, nixosConfig, ... }:
  let
    inherit (lib) mkMerge mkIf optionals;
    waylandEnabled = nixosConfig.system.useWayland;
    kodi = if waylandEnabled then pkgs.kodi-wayland else pkgs.kodi;
    kodiDistro = kodi.withPackages (kpkgs: with kpkgs; [
      inputstreamhelper
      inputstream-adaptive
      inputstream-ffmpegdirect
      inputstream-rtmp
      pvr-iptvsimple
      vfs-sftp
      vfs-libarchive
    ]);
  in {
    home.packages = with pkgs; (mkMerge [
      [
        # calibre # error with python3.9-apsw-3.37.0-r1.drv 2022-04-09
        # firefox-nightly
        openzone-cursors
        ubuntu-themes
        virt-manager
      ][
        # utils
        adb-sync

        # apps
        # albert
        # anydesk
        # aqemu
        aspellDicts.en
        aspellDicts.it
        borgbackup
        # brave
        brightnessctl
        cachix
        chromium
        crip # terminal ripper
        # digikam
        dbeaver-bin
        dpkg

        # brightness of external displays
        ddcutil
        ddcui

        elixir_1_12 erlang mimic
        emacs-pgtk ripgrep fd clang #doom-emacs stuff
        evince
        flac
        fortune
        gimp
        git-crypt
        networkmanagerapplet
        cheese
        google-chrome
        hunspell
        hunspellDicts.en-us
        hunspellDicts.it-it
        inkscape
        ispell
        kodiDistro
        kubectl
        qemu_kvm
        libreoffice
        libvirt
        looking-glass-client
        lyx
        mate.caja
        mate.caja-extensions
        mate.eom
        mc
        nyxt
        oathToolkit
        pavucontrol
        # puddletag
        python3
        # remmina
        samba
        scrcpy
        signal-desktop
        skopeo
        squeezelite
        sqlitebrowser
        (texlive.combine {
          inherit (texlive) scheme-medium collection-langitalian caption
            wrapfig capt-of;
        })
        # thunderbird
        #tor-browser-bundle-bin
        units
        virt-manager
        vlc
        # wine-staging
        # xsettingsd
        # zeal
        rawtherapee
        # visual
        breeze-icons
      ]
      (optionals waylandEnabled [
        (pass-wayland.withExtensions (exts: with exts; [
          pass-genphrase
          pass-import
          pass-otp
        ]))
        wayland
        wdisplays
        wl-clipboard
      ])
      (optionals (! waylandEnabled) [
        arandr
        pass
        redshift
        sakura
      ])
    ]);

    programs = {
      alacritty = mkIf (! waylandEnabled) {
        enable = true;
        settings = {
          env.TERM = "xterm-color";
          font.size = 16;
        };
      };
      foot = {
        enable = true;
        settings = {
          main = {
            term = "xterm-256color";

            font = "monospace:size=10";
            dpi-aware = "yes";
          };
          bell = {
            urgent = true;
            notify = true;
          };
          mouse = {
            hide-when-typing = "yes";
          };
        };
      };
      bash.enable = true;
    };
 }
