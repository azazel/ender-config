# See available options at https://nix-community.github.io/home-manager/options.html
{ config, lib, pkgs, nixosConfig, ... }:
  let
    inherit (lib) mkMerge mkIf optionals;
    waylandEnabled = nixosConfig.system.useWayland;
  in {
    home.sessionVariables = mkMerge [
      {
        EDITOR = "emacsclient";
        FOREIGN_ALIASES_SUPPRESS_SKIP_MESSAGE = 1;
      }
      (mkIf waylandEnabled {
        MOZ_ENABLE_WAYLAND = 1;
        XDG_CURRENT_DESKTOP = "sway";
      })
    ];
  }
