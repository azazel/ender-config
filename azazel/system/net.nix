{config, pkgs, lib, ...}:
  let
    domain = config.networking.domain;
    hostName = config.networking.hostName;
  in {
    networking = rec {
      hosts = {
        "127.0.0.1" = [ "${hostName}.${domain}" "${hostName}" "localhost" ];
        "172.21.200.133" = ["intranet.apss.tn.it" "intranet"];
        "172.21.210.84" = ["intranet-new.apss.tn.it" "intranet-new"];
        "151.101.242.217" = ["cache.nixos.org"];
      };
    };
    services.samba = {
      enable = true;
      settings = {
        public = {
          browseable = "yes";
          comment = "Public samba share.";
          "guest ok" = "yes";
          path = "/srv/public";
          "read only" = true;
        };
      };
    };
}
