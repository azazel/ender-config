# -*- coding: utf-8 -*-
# :Project:   Bean config — Azazel's specific settings at system level
# :Created:   ven 30 ago 2024, 21:57:43
# :Author:    Alberto Berti <alberto@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2024 Alberto Berti
#

{ ... }: {
  imports = [
    ./system/encryption.nix
    ./system/net.nix
    ./system/mounts.nix
    ./system/vpn.nix
  ];
}
