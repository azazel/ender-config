# See available options at https://nix-community.github.io/home-manager/options.html
{ config, lib, pkgs, nixosConfig, ... }: {
  imports = [
    ../azazel/user/programs.nix
    ../azazel/user/session.nix
    ../azazel/user/wayland.nix
    ./programs.nix
  ];
  home.stateVersion = "23.11";
 }
