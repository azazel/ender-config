# See available options at https://nix-community.github.io/home-manager/options.html
{ config, lib, pkgs, nixosConfig, ... }:
  let
    inherit (lib) mkMerge mkIf optionals;
    waylandEnabled = nixosConfig.system.useWayland;
  in {
    home.packages = with pkgs; [
      musescore
    ];
  }
