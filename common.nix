# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./common/hardware-configuration.nix
      ./common/boot.nix
      ./common/net.nix
      ./common/packages.nix
      ./common/xorg.nix
      ./common/window_system.nix
    ];

  console = {
    keyMap = "it";
  };

  environment.homeBinInPath = true;

  #Select internationalisation properties.
  i18n.defaultLocale = "it_IT.UTF-8";
  location = import ./secret/location.nix;

  nix = {
    nixPath = [
      "nixpkgs=${pkgs.path}"
      "/nix/var/nix/profiles/per-user/root/channels"
    ];
    package = pkgs.nixVersions.latest;
    settings = {
      experimental-features = "nix-command flakes";
      sandbox = "relaxed";
      substituters = [
        "https://nix-community.cachix.org/"
        "https://nixpkgs-wayland.cachix.org"
      ];
      trusted-public-keys = [
        "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
        "nixpkgs-wayland.cachix.org-1:3lwxaILxMRkVhehr5StQprHdEo4IrE8sRho9R9HOLYA="
      ];
      trusted-users = [ "root" "azazel" ];
    };
  };


  # nixpkgs.config.permittedInsecurePackages = [
  #   "python3.10-mistune-0.8.4"
  # ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  programs = {
    adb.enable = true;
    command-not-found.enable = true;
    mtr.enable = true;
    sysdig.enable = false;
    xonsh.enable = true;
  };
  # rtkit is optional but recommended
  security.rtkit.enable = true;

  services = {
    avahi = {
      enable = true;
      nssmdns4 = true;
    };
    flatpak.enable = true;
    blueman.enable = true;
    connman.enable = false;
    dbus.packages = [ pkgs.gcr ]; # for gnome3 pinentry
    fstrim.enable = true;
    fwupd.enable = true;
    gvfs.enable = true;
    hardware.bolt.enable = true;
    illum.enable = true;
    journald.extraConfig = ''
      MaxRetentionSec = 4 month
      '';
    openssh.enable = true;
    pcscd.enable = true;
    pcscd.plugins = [ pkgs.ccid pkgs.libacr38u];
    # see https://nixos.wiki/wiki/PipeWire
    pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
      # If you want to use JACK applications, uncomment this
      #jack.enable = true;
      wireplumber.enable = true;
    };
    printing = {
      enable = true;
      drivers = with pkgs; [
        brlaser
        # epson-escpr2
        mfcl3770cdwcupswrapper
        mfcl3770cdwlpr
      ];
    };
    redshift.enable = !config.system.useWayland;
    # teamviewer.enable = true;
  };

  # rootless containers https://rootlesscontaine.rs/getting-started/common/cgroup2/#enabling-cpu-cpuset-and-io-delegation
  # systemd.enableUnifiedCgroupHierarchy = true; # disabled on 2024-08-06:
  # The option definition `systemd.enableUnifiedCgroupHierarchy' in `configuration.nix'
  # no longer has any effect; please remove it. In 256 support for cgroup v1 ('legacy'
  # and 'hybrid' hierarchies) is now considered obsolete and systemd by default will
  # refuse to boot under it. To forcibly reenable cgroup v1 support, you can set
  # boot.kernelParams = [ "systemd.unified_cgroup_hierachy=0" "SYSTEMD_CGROUP_ENABLE_LEGACY_FORCE=1" ].
  # NixOS does not officially support this configuration and might cause your system to be unbootable in future versions. You are on your own.

  systemd.services = {
    docker.serviceConfig = {
      MountFlags = "shared";
    };
    "user@".serviceConfig = {
      Delegate = "cpu cpuset io memory pids";
    };
  };
  # Set your time zone.
  time.timeZone = "Europe/Rome";

  users.users = {
    azazel = {
      isNormalUser = true;
      uid = 1000;
      extraGroups = [ "wheel" "docker" "vboxusers" "cdrom" "video" "libvirtd"
                      "scanner" "lp" "i2c" "input" ];
      createHome = true;
      description = "Alberto Berti";
      shell = pkgs.xonsh;
    };
    emilia = {
      isNormalUser = true;
      createHome = true;
      description = "Emilia Campagna";
      shell = pkgs.xonsh;
    };
  };

  virtualisation = {
    spiceUSBRedirection.enable = true;
    virtualbox.host = {
      enable = false;
      enableExtensionPack = false;
    };
    docker = {
      enable = true;
      extraOptions = lib.concatStringsSep " " [
        "--insecure-registry=10.4.0.76" # E.'s internal registry
      ];
    };
    libvirtd = {
      # it needs "spiceUSBRedirection.enable = true" to redirect USB devices
      enable = true;
    };
    podman = {
      enable = false;
    };
    anbox.enable = false;
    containers.enable = false;
    waydroid.enable = true;
  };

  xdg = {
    portal = {
      enable = true;
      config.common.default = "*";
      extraPortals = with pkgs; [
        xdg-desktop-portal-gtk
      ];
      wlr = {
        enable = true;
      };
    };
  };
}
