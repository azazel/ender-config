{ config, lib, pkgs, ... }:
  let
    inherit (lib) mkMerge optionals;
    waylandEnabled = config.system.useWayland;
  in {
    programs.steam.enable = false;
    nixpkgs.config.allowUnfree = true;
    environment.systemPackages = with pkgs; (mkMerge [
      [
        # utils
        wget
        vim
        less
        tree
        tmux
        zile
        sshfs
        nfs-utils
        cifs-utils
        cpio
        file
        htop
        xfsprogs
        inetutils
        killall
        nmap
        # p7zip # insecure
        openconnect
        pciutils
        pstree
        unzip
        # apps
        gnupg openssl python3 stunnel sysstat tcpdump


        # smarcard
        # pcsctools
        libacr38u opensc
        mate.mate-notification-daemon
        virtiofsd
        xdg-utils
      ]
      (optionals waylandEnabled [
        firefox-wayland
        foot
        glib # gsettings needed in sway's conf
        qt5.qtwayland
      ])
      (optionals (! waylandEnabled) [
        firefox
      ])
    ]);

    fonts.packages =  with pkgs; [
      nerd-fonts.symbols-only # https://www.nerdfonts.com/cheat-sheet
      arkpandora_ttf
      carlito
      dejavu_fonts
      gentium
      gentium-book-basic
      roboto
      roboto-mono
    ];
  }
