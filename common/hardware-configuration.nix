{ config, lib, pkgs, ... }: {

  hardware = {
    pulseaudio = {
      enable = false;
      package = pkgs.pulseaudioFull;
      extraModules =  [ pkgs.pulseaudio-modules-bt ];
      extraConfig = lib.mkForce ''
        load-module module-switch-on-connect
      '';
    };
  };
  nix.settings.max-jobs = lib.mkDefault 8;
  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
}
