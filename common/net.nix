{config, pkgs, lib, ...}: {
  networking = {
    domain = "lan";
    search = [ "lan" ];
    enableIPv6 = false;
    # wireless.enable = true;  # Enables wireless support via wpa_supplicant.
    firewall.enable = false;
  };
}
