{ config, lib, pkgs, ... }: {
  # Enable the X11 windowing system.
  services = {
    # Enable touchpad support.
    libinput.enable = true;

    xserver = {
      enable = false;
      xkb = {
        options = "eurosign:e";
        layout = "it";
      };
      displayManager.startx.enable = true;
    };
  };
}
