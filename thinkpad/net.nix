{config, pkgs, lib, ...}: {
  networking = {
    networkmanager = {
      enable = true;
      enableStrongSwan = true;
      plugins = with pkgs; [
        networkmanager-openvpn
      ];
      wifi = {
        powersave = true;
      };
    };
  };
}
