{ config, lib, pkgs, ... }: {

  hardware = {
    bluetooth = {
      enable = true;
      powerOnBoot = true;
      settings = {
        General = {
          Enable = "Source,Sink,Media,Socket";
        };
      };
    };
    cpu.intel.updateMicrocode = true;
    i2c.enable = true; # allow connected display discovery
    graphics = {
      enable = true;
      extraPackages = with pkgs; [ # The very same list is present in nixos-hardware
        # nix-shell -p libva-utils --run vainfo | grep "Driver version"
        intel-media-driver # LIBVA_DRIVER_NAME=iHD
        vaapiVdpau
        libvdpau-va-gl
      ];
    };
    trackpoint.enable = true;
  };
  environment.etc."libinput/local-overrides.quirks" =  {
    text = ''
      [Trackpoint Override]
      MatchUdevType=pointingstick
      AttrTrackpointMultiplier=0.8
    '';
  };
}
